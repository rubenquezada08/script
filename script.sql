create table perfil(
	id_usuario int identity,
	usuario varchar(20) not null,
)
---------------------------------------------------
create table historial(
	folio int identity,
	id_usuario int not null,
	estatus smallint not null,
	monto_prestamo int not null,
	interes int not null,
	deuda int not null,	
	fecha datetime DEFAULT GETDATE()
)
---------------------------------------------------
create table prestamo(
	folio int not null,
	id_usuario int not null,	
	edad smallint not null,
	tarjeta smallint not null
)
---------------------------------------------------
create table cat_estatus_prestamo(
	estatus int not null,
	descripcion varchar(20) not null,
	primary key(estatus)
)
---------------------------------------------------
create procedure consultaPerfil 
	@_usuario varchar(20)
AS
Begin
	SET NOCOUNT ON;
	select id_usuario, usuario from  perfil where usuario = @_usuario
END

create procedure grabaPerfil 
	@_usuario varchar(20)
AS
Begin
	SET NOCOUNT ON;
	if not exists(select id_usuario, usuario from  perfil where usuario = @_usuario)
	begin
		insert into perfil (usuario) values (@_usuario)
	end
	exec consultaPerfil @_usuario	
END
---------------------------------------------------
create procedure tramitarPrestamo 	
AS
Begin	
	SET NOCOUNT ON;
DECLARE
	@folio INT = 0,
	@id_usuario int = 0,
	@edad smallint = 0,
	@tarjeta smallint = 0,
	
	@estatusAprobado INT = 1,
	@estatusRechazado INT = 2,
	@edadMinima smallint = 20,
	@tarjetaMinima smallint = 1
	
	DECLARE db_cursor CURSOR FOR select folio,id_usuario,edad,tarjeta from prestamo order by folio asc
		OPEN db_cursor;
		FETCH NEXT FROM db_cursor INTO @folio, @id_usuario, @edad, @tarjeta;
		WHILE @@FETCH_STATUS = 0  
		BEGIN
			if(@edadMinima <= @edad and @tarjetaMinima = @tarjeta)
			begin
				update historial set estatus = @estatusAprobado where  folio = @folio and id_usuario = @id_usuario
			end
			else
			begin
				update historial set estatus = @estatusRechazado where  folio = @folio and id_usuario = @id_usuario
			end
			delete from prestamo where  folio = @folio and id_usuario = @id_usuario
			
			FETCH NEXT FROM db_cursor INTO @folio, @id_usuario, @edad, @tarjeta;
		END
		CLOSE db_cursor;
		DEALLOCATE db_cursor;			
END
---------------------------------------------------
create procedure grabaPrestamo 
	@_id_usuario int,
	@_monto int,
	@_interes int,
	@_deuda int,
	@_edad smallint,
	@_tarjeta smallint
AS
Begin	
DECLARE
	@estatusPendiente INT = 0,
	@folio INT = 0

	insert into historial (id_usuario,estatus,monto_prestamo,interes,deuda)
		values(@_id_usuario, @estatusPendiente, @_monto, @_interes,@_deuda)
	select top 1 @folio = folio from  historial where id_usuario = @_id_usuario order by  fecha desc ,folio desc
	insert into prestamo (folio,id_usuario,edad,tarjeta) values (@folio,@_id_usuario,@_edad,@_tarjeta)
		
END
---------------------------------------------------
create procedure consultarHistorial 
	@_id_usuario int
AS
Begin	
	SET NOCOUNT ON;
		
	select folio, id_usuario, estatus, monto_prestamo, interes, deuda, fecha from historial where id_usuario = @_id_usuario order by folio desc ,fecha desc
	
END
---------------------------------------------------
create procedure consultarHistorialPendientes 
	@_id_usuario int
AS
Begin	
	SET NOCOUNT ON;
DECLARE
	@estatusPendiente INT = 0
	
	select folio, id_usuario, estatus, monto_prestamo, interes, deuda, fecha 
	from historial where id_usuario = @_id_usuario and estatus = @estatusPendiente order by folio desc ,fecha desc
	
END